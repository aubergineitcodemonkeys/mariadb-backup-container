#!/bin/bash

olddatevalue=$(date +'%Y%m%d' -d '-7 days')
datevalue=$(date +'%Y%m%d')

# Delete backup from last week if it's there
rm -rf ${MARIABACKUP_BACKUPDIR}/${olddatevalue}

# Delete target folder (for testing? or for live also?)
rm -rf ${MARIABACKUP_BACKUPDIR}/${datevalue}

/usr/bin/mariabackup --backup --compress --compress-threads=4 --host ${MARIABACKUP_HOST} --user=root --target-dir=${MARIABACKUP_BACKUPDIR}/${datevalue}
exit $?
